package com.latihan.Relations.Model;

import javax.persistence.*;

@Entity
public class Chapter {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }



    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    public Chapter(String name) {
        this.name = name;
    }

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "book_id",nullable = true)
    private Book book;


    public Chapter() {
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Column
    private Boolean isDeleted;

}

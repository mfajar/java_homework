package com.latihan.Relations.Model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Book {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Book(String title, List<Chapter> chapters) {
        this.title = title;
        this.chapters = chapters;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Column
    private String author;


    @Column()
    private int year;

    @Column()
    private boolean isDeleted;

    public List<Chapter> getChapter() {
        return chapters;
    }

    public void setChapter(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public Book() {
    }

    @OneToMany(mappedBy = "book",cascade = CascadeType.ALL)
    private List<Chapter> chapters;
}

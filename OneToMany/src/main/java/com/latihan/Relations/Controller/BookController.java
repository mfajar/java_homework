package com.latihan.Relations.Controller;

import com.latihan.Relations.DTO.BookByChapter;
import com.latihan.Relations.DTO.BookDTO;
import com.latihan.Relations.Model.Book;
import com.latihan.Relations.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping()
    public ResponseEntity<BookDTO> createBook(@RequestBody BookDTO bookDTO){
        BookDTO newBook = bookService.createBook(bookDTO);
        return new ResponseEntity<>(newBook, HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<List<BookDTO>> getAllBooks(){
        return new ResponseEntity<>(bookService.getAllBooks(),HttpStatus.OK) ;
    }

    @GetMapping("/chapter")
    public ResponseEntity<List<BookByChapter>> getBookByChapterName(@RequestParam String name){
        return new ResponseEntity<>(bookService.findBooksByChapterName(name),HttpStatus.OK) ;
    }

}

package com.latihan.Relations.Service.Impl;

import com.latihan.Relations.DTO.BookByChapter;
import com.latihan.Relations.DTO.BookDTO;
import com.latihan.Relations.DTO.ChapterDTO;
import com.latihan.Relations.Model.Book;
import com.latihan.Relations.Model.Chapter;
import com.latihan.Relations.Repository.BookRepository;
import com.latihan.Relations.Repository.ChapterRepository;
import com.latihan.Relations.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    ChapterRepository chapterRepository;
    @Override
    public BookDTO createBook(BookDTO bookDTO) {
        Book book = new Book();
        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setYear(bookDTO.getYear());
        book.setDeleted(false);

        List<Chapter> chapterList = new ArrayList<>();
        List<ChapterDTO> chapterDTOList = bookDTO.getChapters();
        for (ChapterDTO chapterDTO:chapterDTOList){
            Chapter chapter = new Chapter();
            chapter.setName(chapterDTO.getName());
            chapter.setContent(chapterDTO.getContent());
            chapter.setDeleted(false);
            chapterList.add(chapter);
        }

        book.setChapter(chapterList);

        Book newBook = bookRepository.save(book);
        for (Chapter newChapter:newBook.getChapter()){
            newChapter.setBook(newBook);
            chapterRepository.save(newChapter);
        }
        return convertBookToBookDTO(newBook) ;
    }

    @Override
    public List<BookDTO> getAllBooks() {
        List<Book> allBook = bookRepository.findAll();
        allBook = allBook.stream().filter(book -> !book.getDeleted()).collect(Collectors.toList());

        List<BookDTO> bookDTOList = new ArrayList<>();
        for (Book book:allBook){
            bookDTOList.add(convertBookToBookDTO(book));
        }
        return bookDTOList;
    }

    @Override
    public List<BookByChapter> findBooksByChapterName(String name) {


        return bookRepository.findBookByChapterName(name);
    }

    private static BookDTO convertBookToBookDTO(Book book){
        BookDTO bookDTO = new BookDTO();
        bookDTO.setId(book.getId());
        bookDTO.setAuthor(book.getAuthor());
        bookDTO.setTitle(book.getTitle());
        bookDTO.setYear(book.getYear());

        List<ChapterDTO> chapterDTOList = new ArrayList<>();
        List<Chapter> bookList = book.getChapter();
        for (Chapter chapter:bookList){
            ChapterDTO chapterDTO = new ChapterDTO();
            chapterDTO.setId(chapter.getId());
            chapterDTO.setContent(chapter.getContent());
            chapterDTO.setName(chapter.getName());
            chapterDTOList.add(chapterDTO);
        }
        bookDTO.setChapter(chapterDTOList);
        return bookDTO;
    }
}

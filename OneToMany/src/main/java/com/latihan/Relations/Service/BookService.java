package com.latihan.Relations.Service;

import com.latihan.Relations.DTO.BookByChapter;
import com.latihan.Relations.DTO.BookDTO;
import com.latihan.Relations.Model.Book;

import java.util.List;

public interface BookService {

    BookDTO createBook(BookDTO bookDTO);
    List<BookDTO> getAllBooks();
    List<BookByChapter> findBooksByChapterName(String name);
}

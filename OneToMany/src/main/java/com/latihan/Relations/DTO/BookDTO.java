package com.latihan.Relations.DTO;

import java.util.List;

public class BookDTO {

    private Long id;


    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    private String author;

    private int year;

    public List<ChapterDTO> getChapters() {
        return chapters;
    }

    public void setChapter(List<ChapterDTO> chapters) {
        this.chapters = chapters;
    }

    private List<ChapterDTO> chapters;
}

package com.latihan.Relations.Repository;

import com.latihan.Relations.DTO.BookByChapter;
import com.latihan.Relations.Model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book,Long> {

    @Query(value = "select new com.latihan.Relations.DTO.BookByChapter(b.title,b.author) from Book b " +
            "JOIN b.chapters c where  c.name = ?1")
    List<BookByChapter> findBookByChapterName(String name);
}
